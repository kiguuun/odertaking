﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OderTaking
{
    public partial class Form2 : Form
    {

        public static Form2 form2Instance;
        

        public Form2()
        {
            InitializeComponent();

            form2Instance = this;
            
        }
        //DataGridViewにデータを追加するメソッド
        public void OderTableAdd(string menuName, 
            int nums, int price, bool isFirst)
        {
            //1回目のデータ追加実行時はif文
            if(isFirst)
            {
                //受け取った引数をDataGridViewの１行に追加
                this.oderDataSet.oderDataTable.AddoderDataTableRow(
                menuName,
                nums,
                price
                );
                //何故か１回目のデータ追加時に空白の１行が追加されるので消去
                this.oderDataGrid.Rows.RemoveAt(1);
                //2回目以降は問題なく動作するのでfalseに変更
                MenuFrom.mainFromInstace.isFirst = false;
            }
            //2回目以降のデータ追加
            else
            {
                //受け取った引数をDataGridViewの１行に追加
                this.oderDataSet.oderDataTable.AddoderDataTableRow(
                menuName,
                nums,
                price
                );
            }
        }

        private void OderDeletionButtonClicked(object sender, EventArgs e)
        {
            //選択した行のデータを削除する
            int row = this.oderDataGrid.CurrentRow.Index;
            this.oderDataGrid.Rows.RemoveAt(row);
          
        }

        private void AccountingButtonClicked(object sender, EventArgs e)
        {
            //合計金額を取得する為に変数を初期化
            int totalPrice = 0;
            
            for(int i = 0; i <this.oderDataGrid.Rows.Count-1;i++)
            {
                //DataGridViewの行数-1だけ繰り返し金額列の値を足していく
                    totalPrice += (int)this.oderDataGrid.Rows[i].Cells[2].Value;
                
            }
            //合計金額に消費税を掛けてint型へキャスト
            int taxPrice = (int)(totalPrice * 1.1);
            //テキストボックスに税抜価格を表示
            this.totalTextBox.Text = @"￥"+totalPrice.ToString();
            //テキストボックスに税込価格を表示
            this.taxPriceBox.Text = @"￥"+taxPrice.ToString();
            //注文の取り消しボタンをクリック無効にする
            this.oderDeletionButton.Enabled = false;
            //メニュー選択画面へ戻るボタンを無効にする
            this.jumpButton.Enabled = false;
            
        }
        
        private void JumpButtonClicked(object sender, EventArgs e)
        {
            //メニュー選択画面へ戻る
            MenuFrom.mainFromInstace.TopMost = true;
        }
    }
}
