﻿namespace OderTaking
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.oderDataGrid = new System.Windows.Forms.DataGridView();
            this.商品名DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.個数DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金額DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oderDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oderDataSet = new OderTaking.OderDataSet();
            this.oderDeletionButton = new System.Windows.Forms.Button();
            this.totalTextBox = new System.Windows.Forms.TextBox();
            this.taxPriceBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.accountButton = new System.Windows.Forms.Button();
            this.jumpButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // oderDataGrid
            // 
            this.oderDataGrid.AutoGenerateColumns = false;
            this.oderDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oderDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.商品名DataGridViewTextBoxColumn,
            this.個数DataGridViewTextBoxColumn,
            this.金額DataGridViewTextBoxColumn});
            this.oderDataGrid.DataSource = this.oderDataTableBindingSource;
            this.oderDataGrid.Location = new System.Drawing.Point(30, 54);
            this.oderDataGrid.Name = "oderDataGrid";
            this.oderDataGrid.RowTemplate.Height = 21;
            this.oderDataGrid.Size = new System.Drawing.Size(538, 239);
            this.oderDataGrid.TabIndex = 0;
            // 
            // 商品名DataGridViewTextBoxColumn
            // 
            this.商品名DataGridViewTextBoxColumn.DataPropertyName = "商品名";
            this.商品名DataGridViewTextBoxColumn.HeaderText = "商品名";
            this.商品名DataGridViewTextBoxColumn.Name = "商品名DataGridViewTextBoxColumn";
            // 
            // 個数DataGridViewTextBoxColumn
            // 
            this.個数DataGridViewTextBoxColumn.DataPropertyName = "個数";
            this.個数DataGridViewTextBoxColumn.HeaderText = "個数";
            this.個数DataGridViewTextBoxColumn.Name = "個数DataGridViewTextBoxColumn";
            // 
            // 金額DataGridViewTextBoxColumn
            // 
            this.金額DataGridViewTextBoxColumn.DataPropertyName = "金額";
            this.金額DataGridViewTextBoxColumn.HeaderText = "金額";
            this.金額DataGridViewTextBoxColumn.Name = "金額DataGridViewTextBoxColumn";
            // 
            // oderDataTableBindingSource
            // 
            this.oderDataTableBindingSource.DataMember = "oderDataTable";
            this.oderDataTableBindingSource.DataSource = this.oderDataSet;
            // 
            // oderDataSet
            // 
            this.oderDataSet.DataSetName = "OderDataSet";
            this.oderDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // oderDeletionButton
            // 
            this.oderDeletionButton.Location = new System.Drawing.Point(30, 319);
            this.oderDeletionButton.Name = "oderDeletionButton";
            this.oderDeletionButton.Size = new System.Drawing.Size(139, 23);
            this.oderDeletionButton.TabIndex = 1;
            this.oderDeletionButton.Text = "注文の取り消し";
            this.oderDeletionButton.UseVisualStyleBackColor = true;
            this.oderDeletionButton.Click += new System.EventHandler(this.OderDeletionButtonClicked);
            // 
            // totalTextBox
            // 
            this.totalTextBox.Location = new System.Drawing.Point(626, 192);
            this.totalTextBox.Name = "totalTextBox";
            this.totalTextBox.Size = new System.Drawing.Size(141, 19);
            this.totalTextBox.TabIndex = 2;
            this.totalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // taxPriceBox
            // 
            this.taxPriceBox.Location = new System.Drawing.Point(626, 274);
            this.taxPriceBox.Name = "taxPriceBox";
            this.taxPriceBox.Size = new System.Drawing.Size(141, 19);
            this.taxPriceBox.TabIndex = 3;
            this.taxPriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(626, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "税抜合計金額";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(626, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "税込合計金額";
            // 
            // accountButton
            // 
            this.accountButton.Location = new System.Drawing.Point(626, 54);
            this.accountButton.Name = "accountButton";
            this.accountButton.Size = new System.Drawing.Size(75, 59);
            this.accountButton.TabIndex = 7;
            this.accountButton.Text = "お会計";
            this.accountButton.UseVisualStyleBackColor = true;
            this.accountButton.Click += new System.EventHandler(this.AccountingButtonClicked);
            // 
            // jumpButton
            // 
            this.jumpButton.Location = new System.Drawing.Point(201, 318);
            this.jumpButton.Name = "jumpButton";
            this.jumpButton.Size = new System.Drawing.Size(160, 23);
            this.jumpButton.TabIndex = 8;
            this.jumpButton.Text = "メニュー選択画面へ戻る";
            this.jumpButton.UseVisualStyleBackColor = true;
            this.jumpButton.Click += new System.EventHandler(this.JumpButtonClicked);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.jumpButton);
            this.Controls.Add(this.accountButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.taxPriceBox);
            this.Controls.Add(this.totalTextBox);
            this.Controls.Add(this.oderDeletionButton);
            this.Controls.Add(this.oderDataGrid);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.oderDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView oderDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn 商品名DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 個数DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金額DataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource oderDataTableBindingSource;
        private OderDataSet oderDataSet;
        private System.Windows.Forms.Button oderDeletionButton;
        private System.Windows.Forms.TextBox totalTextBox;
        private System.Windows.Forms.TextBox taxPriceBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button accountButton;
        private System.Windows.Forms.Button jumpButton;
    }
}