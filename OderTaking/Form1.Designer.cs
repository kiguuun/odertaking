﻿namespace OderTaking
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.oderDataSet = new OderTaking.OderDataSet();
            this.oderDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.oderDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // oderDataSet
            // 
            this.oderDataSet.DataSetName = "OderDataSet";
            this.oderDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // oderDataTableBindingSource
            // 
            this.oderDataTableBindingSource.DataMember = "oderDataTable";
            this.oderDataTableBindingSource.DataSource = this.oderDataSet;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.oderDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oderDataTableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource oderDataTableBindingSource;
        private OderDataSet oderDataSet;
    }
}

