﻿namespace OderTaking
{
    partial class MenuFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.numsTextBox = new System.Windows.Forms.TextBox();
            this.oderButton = new System.Windows.Forms.Button();
            this.menuTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NotEnteredLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(83, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 73);
            this.button1.TabIndex = 0;
            this.button1.Text = "ビール";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.DrinkButton);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(164, 49);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 73);
            this.button2.TabIndex = 1;
            this.button2.Text = "ハイボール";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.DrinkButton);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(245, 49);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 73);
            this.button3.TabIndex = 2;
            this.button3.Text = "チューハイ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.DrinkButton);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(83, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 73);
            this.button4.TabIndex = 3;
            this.button4.Text = "ウーロン茶";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.DrinkButton);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(164, 128);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 73);
            this.button5.TabIndex = 4;
            this.button5.Text = "カルピス";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.DrinkButton);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(245, 128);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 73);
            this.button6.TabIndex = 5;
            this.button6.Text = "オレンジジュース";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.DrinkButton);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(235, 309);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 73);
            this.button7.TabIndex = 8;
            this.button7.Text = "ハラミ";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.FoodButtonClicked);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(154, 309);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 73);
            this.button8.TabIndex = 7;
            this.button8.Text = "ロース";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.FoodButtonClicked);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(73, 309);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 73);
            this.button9.TabIndex = 6;
            this.button9.Text = "カルビ";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.FoodButtonClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "ドリンク";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 291);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "フード";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(701, 108);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 61);
            this.button13.TabIndex = 33;
            this.button13.Text = "-";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.AddSubButtonClicked);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(701, 175);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 61);
            this.button10.TabIndex = 32;
            this.button10.Text = "+";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.AddSubButtonClicked);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(620, 175);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 61);
            this.button11.TabIndex = 31;
            this.button11.Text = "クリア";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.ClearButtonClicked);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(539, 175);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 61);
            this.button12.TabIndex = 30;
            this.button12.Text = "0";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(701, 242);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 61);
            this.button14.TabIndex = 29;
            this.button14.Text = "9";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(620, 242);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 61);
            this.button15.TabIndex = 28;
            this.button15.Text = "8";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(539, 242);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 61);
            this.button16.TabIndex = 27;
            this.button16.Text = "7";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(701, 309);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 61);
            this.button17.TabIndex = 26;
            this.button17.Text = "6";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(620, 309);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 61);
            this.button18.TabIndex = 25;
            this.button18.Text = "5";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(539, 309);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 61);
            this.button19.TabIndex = 24;
            this.button19.Text = "4";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(701, 376);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 61);
            this.button20.TabIndex = 23;
            this.button20.Text = "3";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(620, 376);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 61);
            this.button21.TabIndex = 22;
            this.button21.Text = "2";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(539, 376);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 61);
            this.button22.TabIndex = 21;
            this.button22.Text = "1";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.NumsButtonClicked);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(410, 309);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 35;
            this.label7.Text = "個数";
            // 
            // numsTextBox
            // 
            this.numsTextBox.Enabled = false;
            this.numsTextBox.Location = new System.Drawing.Point(410, 336);
            this.numsTextBox.Name = "numsTextBox";
            this.numsTextBox.Size = new System.Drawing.Size(100, 19);
            this.numsTextBox.TabIndex = 34;
            // 
            // oderButton
            // 
            this.oderButton.Location = new System.Drawing.Point(412, 395);
            this.oderButton.Name = "oderButton";
            this.oderButton.Size = new System.Drawing.Size(98, 23);
            this.oderButton.TabIndex = 36;
            this.oderButton.Text = "注文リストへ追加";
            this.oderButton.UseVisualStyleBackColor = true;
            this.oderButton.Click += new System.EventHandler(this.OderAddButtonClicked);
            // 
            // menuTextBox
            // 
            this.menuTextBox.Enabled = false;
            this.menuTextBox.Location = new System.Drawing.Point(410, 274);
            this.menuTextBox.Name = "menuTextBox";
            this.menuTextBox.Size = new System.Drawing.Size(100, 19);
            this.menuTextBox.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(412, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 38;
            this.label3.Text = "商品名";
            // 
            // NotEnteredLabel
            // 
            this.NotEnteredLabel.AutoSize = true;
            this.NotEnteredLabel.Location = new System.Drawing.Point(410, 377);
            this.NotEnteredLabel.Name = "NotEnteredLabel";
            this.NotEnteredLabel.Size = new System.Drawing.Size(100, 12);
            this.NotEnteredLabel.TabIndex = 39;
            this.NotEnteredLabel.Text = "未入力欄があります";
            this.NotEnteredLabel.Visible = false;
            // 
            // MenuFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 456);
            this.Controls.Add(this.NotEnteredLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.menuTextBox);
            this.Controls.Add(this.oderButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numsTextBox);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "MenuFrom";
            this.Text = "menuFrom";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MenuFormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox numsTextBox;
        private System.Windows.Forms.Button oderButton;
        private System.Windows.Forms.TextBox menuTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label NotEnteredLabel;
    }
}