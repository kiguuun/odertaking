﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OderTaking
{
    class Menu
    {
        private string name;
        private int price;

        public Menu(string name)
        {
            this.name = name;
            switch(this.name)
            {
                case "ビール":
                    this.price = 420;
                    break;
                case "ハイボール":
                    this.price = 400;
                    break;
                case "チューハイ":
                    this.price = 350;
                    break;
                case "ウーロン茶":
                case "オレンジジュース":
                case "カルピス":
                    this.price = 200;
                    break;
                case "カルビ":
                    this.price = 800;
                    break;
                case "ロース":
                    this.price = 820;
                    break;
                case "ハラミ":
                    this.price = 850;
                    break;
            }
    
        }

        public string Name { get => name; set => name = value; }
        public int Price { get => price; set => price = value; }
    }
}
