﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OderTaking
{
    public partial class Form1 : Form
    {
        MenuFrom menuFrom;

        public static Form1 form1Instance;

        public Form1()
        {
            InitializeComponent();
            this.menuFrom = new MenuFrom();
            menuFrom.Show();
            WindowState = FormWindowState.Minimized;

            form1Instance = this;
            
        }
    }
}
