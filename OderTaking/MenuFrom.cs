﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OderTaking
{
    public partial class MenuFrom : Form
    {
        Menu menu;
        //Form2 form2;
        //自身のインスタンスを持たす事でどのフォームからも呼べる
        public static MenuFrom mainFromInstace;
        //DataGridViewにデータを追加するとき何故か１回目だけ
        //２行目に空白が入るので１回目のデータ入力か判定するisFirst
        public bool isFirst;
        public MenuFrom()
        {
            InitializeComponent();
            Form2.form2Instance = new Form2();
            Form2.form2Instance.Show();
            //自身のインスタンス生成
            mainFromInstace = this;
            isFirst = true;
           
        }

        private void DrinkButton(object sender, EventArgs e)
        {
            //選択されたドリンクボタンの名前を表示
            Button button = (Button)sender;
            this.menuTextBox.Text = button.Text;
            
        }

        private void NumsButtonClicked(object sender, EventArgs e)
        {
            //注文の個数を受け付ける
            Button button = (Button)sender;
            if(this.numsTextBox.Text == null || this.numsTextBox.Text =="0")
            {
                //個数テキストボックスに０もしくは何もなければ
                //選んだボタンの数を表示
                this.numsTextBox.Text = button.Text;
            }
            else
            {
                //既に入力されていれば文字連結
                this.numsTextBox.Text =
                    this.numsTextBox.Text + button.Text;
            }
        }

        private void OderAddButtonClicked(object sender, EventArgs e)
        {
            //未入力欄ラベルを非表示にする
            this.NotEnteredLabel.Visible = false;
            //テキストボックスに空白が無いときだけDataGridViewに表示
            if (this.menuTextBox.Text != null &&
                this.menuTextBox.Text.Length != 0 &&
                this.numsTextBox.Text.Length !=0 && 
                this.numsTextBox.Text != "0")
            {
                //注文が確定したらメニューのインスタンスを生成
                this.menu = new Menu(this.menuTextBox.Text);
                //受け付けた注文の品名、個数、価格を取得
                string menuName = menu.Name;
                int nums = int.Parse(this.numsTextBox.Text);
                int price = menu.Price * nums;
                //データテーブルに情報をセットする
                Form2.form2Instance.OderTableAdd(menuName, nums, price, isFirst);

                //各テキストボックスに入っている値を初期化
                this.menuTextBox.Text = null;
                this.numsTextBox.Text = null;
                //注文一覧画面を表示
                Form2.form2Instance.TopMost = true;
            }else
            {
                //未入力欄ラベルを表示
                this.NotEnteredLabel.Visible = true;
            }

        }
        

        private void FoodButtonClicked(object sender, EventArgs e)
        {
            //押されたフードボタンの名前を表示
            Button button = (Button)sender;
            this.menuTextBox.Text = button.Text;
            
        }

        private void ClearButtonClicked(object sender, EventArgs e)
        {
            //各テキストボックスに入っている値を初期化
            this.menuTextBox.Text = null;
            this.numsTextBox.Text = null;
        }

        //注文の個数を増減する
        private void AddSubButtonClicked(object sender, EventArgs e)
        {
            //押されたボタンの情報を取得
            Button button = (Button)sender;
            //個数テキストボックスに表示されている値をint型に変換
            int nums = int.Parse(this.numsTextBox.Text);
            
            if(this.numsTextBox.Text != null &&
                this.numsTextBox.Text !="0")
            {
                //+ボタンか-ボタンを判定
                switch(button.Text)
                {
                    case "+":
                        nums++;
                        break;
                    case "-":
                        nums--;
                        break;
                }
            }
            //増減された値を個数テキストボックスに代入
            this.numsTextBox.Text = nums.ToString();
        }

        private void MenuFormClosed(object sender, FormClosedEventArgs e)
        {
            //MenuFormの×ボタンを押すとアプリケーションが終了
            Form1.form1Instance.Close();
        }
    }
}
